/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
	getUser: function(req, res) {
		User.find({}, function(err, user) {
			

			if (err)
				return res.status(500).json( {
					error: err,
					message: "Something went wrong when finding trades"
				});

			return res.status(200).json({
				user: user
			});
		});
	},
	postUser:async function(req,res){
		await User.create({name:'Finn'});
        sails.sockets.blast('/getUser', {
          verb: 'created'
          
        })

		return res.ok();

	}
};
